# Using XOM
*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the jar files.*

*For example `DeltaXML-XML-Compare-10_0_0_j/samples/sample-name`.*

---

A simple code sample showing how to use in memory XOM structures as inputs/outputs.

This document describes how to run the sample. For concept details see: [Using XOM](https://docs.deltaxml.com/xml-compare/latest/samples-and-guides/using-xom)

The sample code essentially loads two input files (`input1.xml` and `input2.xml`) into XOM documents, compares them, produces an output XOM document, which is then saved to a file (`output.xml`). 
The sample is designed to be built and run via the Ant build technology. The provided *build.xml* script has two main targets:

- run - the default target which compiles and runs the sample code.
- clean - returns the sample to its original state.

It can be run by issuing either the `ant -Dxom-jar=<path-to-xom.jar>` or the `ant -Dxom-jar=<path-to-xom.jar> run` commands, where *<path-to-xom.jar>* is the path to the user supplied XOM library.
Alternatively, the sample can be manually compiled and run using the following Java commands, assuming that both the Java compiler and runtime platforms are available on the command-line, and that <path-to-xom.jar> is the path to the user supplied XOM library.
Replace x.y.z with the major.minor.patch version number of your release e.g. `deltaxml-10.0.0.jar`:

	javac -cp ../../deltaxml-x.y.z.jar:../../saxon9pe.jar:<path-to-xom.jar> UsingXOM.java XOMDestination.java
	java -cp .:../../deltaxml-x.y.z.jar:../../saxon9pe.jar:<path-to-xom.jar> UsingXOM input1.xml input2.xml output.xml

Note that you need to ensure that you use the correct directory and class path separators for your operating system.