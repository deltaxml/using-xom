// Copyright (c) 2024 Deltaman group limited. All rights reserved.

import net.sf.saxon.Configuration;
import net.sf.saxon.event.PipelineConfiguration;
import net.sf.saxon.event.Receiver;
import net.sf.saxon.option.xom.XOMWriter;
import net.sf.saxon.s9api.Action;
import net.sf.saxon.s9api.Destination;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.serialize.SerializationProperties;
import nu.xom.Document;

import java.net.URI;

/**
 * Create a Saxon Destination that can be used to create XOM documents.
 */
public class XOMDestination implements Destination {

  /**
   * The receiver/result used by Saxon to write the output to.
   */
  private XOMWriter xomWriter;

  /**
   * Create a XOMDestination.
   */
  public XOMDestination(Configuration config) {
    xomWriter= new XOMWriter(config.makePipelineConfiguration());
  }

  /**
   * Get the document node of the constructed XOM tree. Note this will return null prior to the destination being used by Saxon.
   *
   * @return
   */
  public Document getDocument() {
    return xomWriter.getDocument();
  }


  @Override
  public void setDestinationBaseURI(URI uri) {

  }

  @Override
  public URI getDestinationBaseURI() {
    return null;
  }

  /**
   * Return a Receiver. Saxon calls this method to obtain a Receiver, to which it then sends a sequence of events representing the
   * content of an XML document.
   *
   * @param pipelineConfiguration The Saxon pipeline configuration. This is supplied so that the destination can use information from the configuration
   *          (for example, a reference to the name pool) to construct or configure the returned Receiver.
   * @param serializationProperties if any
   * @return the Receiver to which events are to be sent, which will always be a XomWriter.
   * @throws SaxonApiException if the Receiver cannot be created
   */
  @Override
  public Receiver getReceiver(PipelineConfiguration pipelineConfiguration, SerializationProperties serializationProperties) throws SaxonApiException {
    xomWriter.setPipelineConfiguration(pipelineConfiguration);
    return xomWriter;
  }

  @Override
  public void onClose(Action action) {

  }

  @Override
  public void closeAndNotify() throws SaxonApiException {

  }

  /**
   * Close the destination, allowing resources to be released. Saxon calls this method when it has finished writing to the
   * destination.
   */
  public void close() throws SaxonApiException {
    // no action
  }
}
