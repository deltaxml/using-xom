// Copyright (c) 2024 Deltaman group limited. All rights reserved.

// $Id$

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import net.sf.saxon.Configuration;
import net.sf.saxon.option.xom.XOMDocumentWrapper;
import net.sf.saxon.option.xom.XOMWriter;
import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.XdmNode;
import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.ParsingException;
import nu.xom.Serializer;
import nu.xom.ValidityException;

import com.deltaxml.core.ComparatorInstantiationException;
import com.deltaxml.core.FeatureNotRecognizedException;
import com.deltaxml.core.ParserInstantiationException;
import com.deltaxml.cores9api.ComparisonException;
import com.deltaxml.cores9api.FilterProcessingException;
import com.deltaxml.cores9api.LicenseException;
import com.deltaxml.cores9api.PipelinedComparatorS9;
import com.deltaxml.cores9api.ComparisonCancelledException;

/**
 * A class that illustrates how to use XOM models in comparison provided by the com.deltaml.cores9api package.
 */
public class UsingXOM {

  /**
   * The XOM document builder used to load existing documents.
   */
  private final Builder xomBuilder;

  /**
   * Create the Using XOM sample comparison object.
   */
  public UsingXOM() {
    xomBuilder= new nu.xom.Builder();
  }

  /**
   * The XOM document comparison method.
   *
   * @param doc1 The first document to compare.
   * @param systemId1 The first document's system id (i.e. file location).
   * @param doc2 The second document to compare.
   * @param systemId2 The second document's system id (i.e. file location).
   * @return The document that contains the result of the comparison.
   * @throws ParserInstantiationException thrown when there is a problem instantiating or configuring a SAX parser.
   * @throws ComparatorInstantiationException thrown when there is a problem instantiating or configuring a DeltaXML Comparator.
   * @throws IllegalArgumentException thrown when an illegal argument is detected.
   * @throws FeatureNotRecognizedException thrown when a comparison feature is not recognised.
   * @throws SaxonApiException thrown thrown when there is a problem detected by Saxon's API
   * @throws FilterProcessingException thrown when there is a problem running a pipeline filter.
   * @throws ComparisonException thrown when there is a problem performing the comparison.
   * @throws LicenseException thrown when there is a problem with DeltaXML license.
   */
  public Document compare(Document doc1, String systemId1, Document doc2, String systemId2) throws
      ParserInstantiationException, ComparatorInstantiationException, IllegalArgumentException, FeatureNotRecognizedException,
      SaxonApiException, FilterProcessingException, ComparisonException, LicenseException, ComparisonCancelledException {

    // Construct and configure the DeltaXML Saxon PE processor, though the PE features are only available inside the comparison
    // engine due to licensing conditions.
    Processor proc= new Processor(true);
    Configuration config= proc.getUnderlyingConfiguration();
    // Saxon documentation suggests that a XOM model needs to be registered, however this does not appear to be the case for
    // Saxon-PE-9.3. To register the XOM object model uncomment the following line.
    // config.registerExternalObjectModel(XOMObjectModel.getInstance());

    // Create and configure a DeltaXML (s9api) pipelined comparator
    PipelinedComparatorS9 pc= new PipelinedComparatorS9(proc);
    pc.setComparatorFeature("http://deltaxml.com/api/feature/isFullDelta", true);
    pc.setComparatorFeature("http://deltaxml.com/api/feature/deltaV2", true);

    // Appropriately prepare the inputs for comparison using Saxon XdmNodes
    XOMDocumentWrapper wrap1= new XOMDocumentWrapper(doc1, config);
    XOMDocumentWrapper wrap2= new XOMDocumentWrapper(doc2, config);
    XdmNode xdmNode1= proc.newDocumentBuilder().build(wrap1);
    XdmNode xdmNode2= proc.newDocumentBuilder().build(wrap2);

    // Perform the comparison
    XdmNode result= pc.compare(xdmNode1, xdmNode2);

    // Convert the result to a XOM document and return it.
    XOMDestination dest= new XOMDestination(config);
    proc.writeXdmValue(result, dest);
    XOMWriter writer= (XOMWriter)dest.getReceiver(config.makePipelineConfiguration(), null);
    return writer.getDocument();
  }

  /**
   * Loading an existing document from a byte stream.
   *
   * @param in The input byte stream.
   * @return A XOM document.
   * @throws ValidityException thrown when there is a problem with the XOM validation and the builder is in validation mode.
   * @throws ParsingException thrown when there is a problem parsing the XML.
   * @throws IOException throw when there is a problem with the IO handling.
   */
  public Document loadDocument(InputStream in) throws ValidityException, ParsingException, IOException {
    if (!(in instanceof BufferedInputStream)) {
      in= new BufferedInputStream(in);
    }
    return xomBuilder.build(in);
  }

  /**
   * Save the result of the comparison.
   *
   * @param doc The document to save.
   * @param out The output byte stream.
   * @throws IOException throw when there is a problem with the IO handling.
   */
  public void saveDocument(Document doc, OutputStream out) throws IOException {
    Serializer serializer= new Serializer(out);
    serializer.write(doc);
  }

  /**
   * The main program.
   *
   * @param args Two input files and an output file.
   * @throws ValidityException thrown when there is a problem with the XOM validation and the builder is in validation mode.
   * @throws ParsingException thrown when there is a problem parsing the XML.
   * @throws IOException throw when there is a problem with the IO handling.
   * @throws FilterProcessingException thrown when there is a problem running a pipeline filter.
   * @throws ComparisonException thrown when there is a problem performing the comparison.
   * @throws LicenseException thrown when there is a problem with DeltaXML license.
   * @throws ParserInstantiationException thrown when there is a problem instantiating or configuring a SAX parser.
   * @throws ComparatorInstantiationException thrown when there is a problem instantiating or configuring a DeltaXML Comparator.
   * @throws FeatureNotRecognizedException thrown when a comparison feature is not recognised.
   * @throws IllegalArgumentException thrown when an illegal argument is detected.
   * @throws SaxonApiException thrown thrown when there is a problem detected by Saxon's API
   */
  public static void main(String[] args) throws ValidityException, ParsingException, IOException,
          FilterProcessingException, ComparisonException, LicenseException, ParserInstantiationException,
          ComparatorInstantiationException, FeatureNotRecognizedException, IllegalArgumentException, SaxonApiException, ComparisonCancelledException {

    if (args.length != 3) {
      System.out.println();
      System.out.println("Usage:");
      System.out.println("  java -cp <CP> UsingXOM <input-file-1> <input-file-2> <output-file>");
      System.out.println("where <CP> is");
      System.out.println("  ../../deltaxml.jar:../../saxon9pe.jar:<path-to-xom.jar>");
      System.out.println();
      System.exit(1);
    }
    UsingXOM usingXOM= new UsingXOM();

    Document doc1= usingXOM.loadDocument(new FileInputStream(args[0]));
    Document doc2= usingXOM.loadDocument(new FileInputStream(args[1]));

    Document doc3= usingXOM.compare(doc1, args[0], doc2, args[1]);

    usingXOM.saveDocument(doc3, new FileOutputStream(args[2]));
  }
}
